from django.core.exceptions import ValidationError
from django.db import models, IntegrityError
from django.contrib.auth.models import AbstractUser
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from django.core.validators import validate_email, EmailValidator


# Create your models here.


class User(AbstractUser):
    """ Custom User Model """

    email = models.EmailField(
        _('email address'),
        null=False,
        blank=False,
        default=None,
        validators=[
            validate_email,
        ],
        unique=True,
    )
    slug = models.SlugField(null=True, blank=True)
    username = models.CharField(
        _("username"),
        max_length=150,
        null=False,
        blank=False,
        unique=True,
        default=None,
        help_text=_(
            "Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."
        ),
        error_messages={
            "unique": _("A user with that username already exists."),
        },
    )

    last_name = models.CharField(
        _("last name"), max_length=150, blank=False, null=False, default=None)

    def save(self, *args, **kwargs):
        try:
            validate_email(self.email)
        except ValidationError as error:
            raise IntegrityError(error.message)
        self.slug = slugify(self.get_full_name())
        super().save(*args, **kwargs)
